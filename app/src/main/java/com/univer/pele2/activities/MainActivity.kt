package com.univer.pele2.activities

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.univer.pele2.News
import com.univer.pele2.R
import com.univer.pele2.TasksStartParams
import com.univer.pele2.fragments.DetailFragment
import com.univer.pele2.fragments.PagerFragment
import com.univer.pele2.fragments.TasksFragment

const val REQUEST_CODE = 1

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        when(resources.configuration.orientation) {

            Configuration.ORIENTATION_PORTRAIT -> {

                val fragment = TasksFragment.newInstance(TasksStartParams(false))
                supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentHolder, fragment)
                .addToBackStack(null)
                .commit()
            }

            Configuration.ORIENTATION_LANDSCAPE -> {
                val fragment1 = TasksFragment.newInstance(TasksStartParams(true))
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHolderFirst, fragment1)
                    .addToBackStack(null)
                    .commit()
                val fragment2 = DetailFragment.newInstance(TasksStartParams(isLandscape =  true, isNewTask = true))
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHolderSecond, fragment2)
                    .addToBackStack(null)
                    .commit()
            }
        }
    }

    fun openTasks(){

    }

    fun openDetail(news: News? = null, isNewTask : Boolean, isLandscape : Boolean){
        val fragment = DetailFragment.newInstance(TasksStartParams(isLandscape, isNewTask, news))
        if (isLandscape)
        {
            if (!isNewTask) {
                //supportFragmentManager.popBackStack(f)
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHolderSecond, fragment)
                    .addToBackStack(null)
                    .commit()
            }
            else
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHolderSecond, fragment)
                    .addToBackStack(null)
                    .commit()
        }
        else {
            val fragment1 = PagerFragment.newInstance(TasksStartParams(news = news, isNewTask = true))
            val fragment2 = DetailFragment.newInstance(TasksStartParams(news = news, isNewTask = true))
            if (!isNewTask){
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHolder, fragment1)
                    .addToBackStack(null)
                    .commit()
            }
            else
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentHolder, fragment2)
                    .addToBackStack(null)
                    .commit()
        }
    }

    fun addOrSave(){

    }
}
