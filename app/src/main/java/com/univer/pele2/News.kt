package com.univer.pele2

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.util.*

data class News(
    @SerializedName("id") val Id: Int,
    @SerializedName("name") val name: String?,
    @SerializedName("comment")val comment: String?,
    @SerializedName("date") val date: String?,
    @SerializedName("isValid") val isValid: Boolean,
    @SerializedName("checks") val checks: ArrayList<Check>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.createTypedArrayList(Check.CREATOR)
        /*arrayListOf<Check>().apply {
            parcel.readList(this.toList(), Check::class.java.classLoader)
        }*/
    )
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(Id)
        parcel.writeString(name)
        parcel.writeString(comment)
        parcel.writeString(date)
        parcel.writeByte(if (isValid) 1 else 0)
        parcel.writeTypedList(checks)
        /*if (Build.VERSION.SDK_INT >= 29) {
            parcel.writeParcelableList(Check,flags)
        } else {
            parcel.writeList(check as List<Check>)
        }*/
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<News> {
        override fun createFromParcel(parcel: Parcel): News {
            return News(parcel)
        }

        override fun newArray(size: Int): Array<News?> {
            return arrayOfNulls(size)
        }
    }
}