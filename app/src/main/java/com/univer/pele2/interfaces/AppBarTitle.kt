package com.univer.pele2.interfaces

interface TitleNameListener {
    fun setAppBarTitle(title: String)
}