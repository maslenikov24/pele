package com.univer.pele2

data class TasksStartParams(
    val isLandscape: Boolean = false,
    val isNewTask: Boolean = false,
    val news: News? = null
)