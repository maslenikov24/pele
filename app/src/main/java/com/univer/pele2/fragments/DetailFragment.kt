package com.univer.pele2.fragments

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import com.univer.pele2.*
import com.univer.pele2.activities.MainActivity
import com.univer.pele2.extentions.visible
import com.univer.pele2.interfaces.TitleNameListener
import kotlinx.android.synthetic.main.activity_content.*
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class DetailFragment : BaseFragment(), DatePickerDialog.OnDateSetListener{

    override val layoutRes = R.layout.activity_content

    companion object{

        private const val IS_LANDSCAPE = "isLandscape"
        private const val IS_NEW_TASK = "isNewTask"
        private const val OBJECT = "object"

        fun newInstance(startParams: TasksStartParams) : DetailFragment =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(IS_LANDSCAPE, startParams.isLandscape)
                    putBoolean(IS_NEW_TASK, startParams.isNewTask)
                    putParcelable(OBJECT, startParams.news)
                }
            }
    }

    //private val activityMain = activity as MainActivity

    private val adapter = FeedAdapter()

    private var delegate : TitleNameListener?= null

    private val isLandscape get() = arguments?.getBoolean(IS_LANDSCAPE) ?: false

    private val isNewTask get() = arguments?.getBoolean(IS_NEW_TASK) ?: false

    private val news get() = arguments?.getParcelable<News>(OBJECT)

    private var name : String? = null
    private  var comment : String? = null
    private var date : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        if (!isLandscape) news?.let { (parentFragment as TitleNameListener).setAppBarTitle(it.name.orEmpty()) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //news?.let { delegate?.setAppBarTitle(it.name.orEmpty()) }
        //news?.let { (parentFragment as TitleNameListener).setAppBarTitle(it.name.orEmpty()) }


        buttonAdd.text = "Сохранить"
        buttonAdd.setOnClickListener {
            /*(activity as MainActivity).addOrSave(news, isNewTask)
            JsonHelperInteractor*/
            name = label_name.text.toString()
            comment = label_comment.text.toString()
            date = label_date.text.toString()
            val mList = JsonHelperInteractor(requireContext()).open()
            if (!name.isNullOrEmpty() && !date.isNullOrEmpty()) {
                if (!isNewTask) {
                    news?.let {
                        //if (mList?.equals(it)!!) {
                        mList?.set(mList.indexOf(it), News(
                            Id = it.Id, name = name, comment = comment, date = date, isValid = it.isValid, checks = null
                        )
                        )
                        //}
                    }
                    //val itemId = news?.Id
                    /*mList?.withIndex()?.forEach {
                        if (it.value.Id == itemId) {
                            news?.let { new->
                                mList[it.index] = new
                            }
                            return@forEach
                        }
                    }*/
                }
                    else {
                            mList?.add(News(Id = mList.size, name = name, comment = comment, date = date, isValid = true, checks = null)
                            )
                }
                mList?.let {
                    it.sortBy { item -> !item.isValid }
                    JsonHelperInteractor(requireContext()).save(it)
                    (activity as MainActivity).supportFragmentManager.popBackStack()
                    adapter.setData(it)
                }
            }
            else Toast.makeText(requireContext(), "Задача не сохранена", Toast.LENGTH_LONG).show()
        }

        if (!isLandscape) {
            with(activity as MainActivity) {

                /*setSupportActionBar(appBar)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                supportActionBar?.setDisplayShowHomeEnabled(true)
                supportActionBar?.setDisplayShowTitleEnabled(false)
                this@DetailFragment.appBar.setNavigationOnClickListener {
                    onBackPressed()
                }
                this@DetailFragment.appBar.setOnMenuItemClickListener { item ->
                    when {
                        item.itemId == R.id.home -> {
                            //onBackPressed()
                        }
                        else -> {
                            // do something
                        }
                    }

                    false
                }*/
            }
        }
        //activityMain.intent

        if (news != null){
            name = news?.name.toString()
            comment = news?.comment.toString()
            date = news?.date
            //checks = news?.checks?.toMutableList()

            label_name.setText(name)
            label_comment.setText(comment)
            label_date.setText(date)
            appBar.title = name

            /*hecks?.forEach {
                addCheckBox(it.name.orEmpty(), it.isChecked)
            }*/


            val answer = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")// HH:mm")
                news?.date?.format(formatter)
            } else {
                val formatter = SimpleDateFormat("MMM dd yyyy")// HH:mma")
                formatter.format(date)
            }

            label_date.setText(answer)
        }
        else appBar.title = "Новая задача"

        label_date.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showDateTimePicker(date)
            }
        }

        /*fab.setOnClickListener {
            showAlertDialog(it)
        }*/

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(view){
            appBarLayout.visible(!isLandscape)
        }
    }

    private fun attachDelegate(delegate : TitleNameListener){
        this.delegate = delegate
    }

    private fun showDateTimePicker(date: String?){
        val year = date?.split(".")?.get(2)?.toInt()
        val month = date?.split(".")?.get(1)?.toInt()
        val day = date?.split(".")?.get(0)?.toInt()
        DatePickerDialog(
            requireContext(),
            this,
            year ?: Calendar.getInstance().get(Calendar.YEAR),
            month ?: Calendar.getInstance().get(Calendar.MONTH),
            day ?: Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        label_date.setText("$dayOfMonth.$month.$year")
    }
}