package com.univer.pele2

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.*


internal object JSONHelper {

    private const val FILE_NAME = "data.json"

    fun exportToJSON(dataList: MutableList<News>, context: Context): Boolean {

        val jsonString = Gson().toJson(dataList)
        var fileOutputStream: FileOutputStream? = null

        try {
            fileOutputStream = context.openFileOutput(FILE_NAME, MODE_PRIVATE)
            fileOutputStream!!.write(jsonString.toByteArray())
            return true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }

        return false
    }

    fun importFromJSON(context: Context): MutableList<News>? {

        var streamReader: InputStreamReader? = null
        var fileInputStream: InputStream? = null
        try {
            fileInputStream = context.openFileInput(FILE_NAME)
            streamReader = InputStreamReader(fileInputStream!!)
            val typeToken = object : TypeToken<List<News>>(){}.type
            val dataItems = Gson().fromJson<List<News>>(streamReader, typeToken)
            return dataItems.toMutableList()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: java.lang.Exception){
            e.printStackTrace()
        }
        finally {
            if (streamReader != null) {
                try {
                    streamReader.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }

        return null
    }
}