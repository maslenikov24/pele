package com.univer.pele2

import android.content.Context
import android.widget.Toast

class JsonHelperInteractor (private val context: Context) {

    fun save(data: MutableList<News>) {

        val result = JSONHelper.exportToJSON(data, context)
        if (result) {
            //Toast.makeText(context, "Данные сохранены", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, "Не удалось сохранить данные", Toast.LENGTH_LONG).show()
        }
    }

    fun open() : MutableList<News>? =
        try{
            val mList = JSONHelper.importFromJSON(context).orEmpty().toMutableList()
            //Toast.makeText(context, "Данные восстановлены", Toast.LENGTH_LONG).show()
            mList
        }
        catch(e: Exception){
            Toast.makeText(context, "Не удалось открыть данные", Toast.LENGTH_LONG).show()
            null
        }

}