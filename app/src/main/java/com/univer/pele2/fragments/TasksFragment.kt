package com.univer.pele2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.univer.pele2.extentions.visible
import kotlinx.android.synthetic.main.activity_feed.*
import android.app.Activity
import com.univer.pele2.*
import com.univer.pele2.activities.ContentActivity
import com.univer.pele2.activities.MainActivity
import com.univer.pele2.activities.REQUEST_CODE


class TasksFragment : BaseFragment(){

    override val layoutRes = R.layout.activity_feed

    companion object {
        private const val IS_LANDSCAPE = "isLandscape"

        fun newInstance(startParams: TasksStartParams) = TasksFragment().apply {
            arguments = Bundle().apply {
                putBoolean(IS_LANDSCAPE, startParams.isLandscape)
            }
        }
    }

    private val isLandscape get()  = arguments?.getBoolean(IS_LANDSCAPE) ?: false

    private var adapter = FeedAdapter()

    private var mList = mutableListOf<News>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        refresh()
        appBarLayout.visible(!isLandscape)
        appBar.inflateMenu(R.menu.toolbar_menu)
        appBar.menu.getItem(1).isVisible = true
        appBar.setOnMenuItemClickListener { item ->
            when {
                item.itemId == R.id.action_removeAll -> {
                    removeAll()
                }
                else -> { /*do something*/ }
            }
            false
        }
        mList = JsonHelperInteractor(requireContext()).open()?: mutableListOf()
        if (mList.size == 0) addTask.visible(true)

        fab.setOnClickListener {
            (activity as MainActivity).openDetail(isNewTask =  true, isLandscape =  isLandscape)
        }

        recycler.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@TasksFragment.adapter
            setHasFixedSize(true)
        }

        adapter.setData(mList)

        adapter.attachDelegate(object: FeedDelegate {

            override fun isRemoveTask(item: News, position: Int) {
                showChooseDialog(item, position)
            }

            override fun openDetail(news: News) {
                (activity as MainActivity).openDetail(news, false, isLandscape)
            }

            override fun completeChange(news: News, isChecked: Boolean, position: Int) {
                val new = News(
                    news.Id,
                    news.name,
                    news.comment,
                    news.date,
                    !news.isValid,
                    news.checks
                )
                mList.removeAt(position)
                adapter.removeDataWithPosition(position)
                mList.add(new)


                mList.sortBy { item -> !item.isValid }
                var existsInvalid = false
                mList.withIndex().forEach {
                    if (!it.value.isValid){
                        existsInvalid = true
                        if (news.isValid) adapter.setDataWithPosition(new, it.index)
                        else adapter.setDataWithPosition(new, it.index-1)
                        JsonHelperInteractor(requireContext()).save(mList)
                        return
                    }
                }
                if (!existsInvalid) adapter.setDataWithPosition(new, mList.lastIndex)
            }
        })
    }

    private fun removeAll(){
        addTask.visible(true)
        mList = mutableListOf()
        JsonHelperInteractor(requireContext()).save(mutableListOf())
        adapter.setData(mutableListOf())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data!= null){
            val new = data.getParcelableExtra<News>("object")
            if (!new?.name.isNullOrEmpty() && !new?.date.isNullOrEmpty()) {
                when (resultCode) {
                    1 -> {
                        val item = data.getParcelableExtra<News>("object")
                        val itemId = item?.Id
                        mList.withIndex().forEach {
                            if (it.value.Id == itemId) {
                                mList[it.index] = item
                                return@forEach
                            }
                        }
                    }
                    0 -> {
                        val obj = data.getParcelableExtra<News>("object")
                        obj?.let {
                            mList.add(
                                News(
                                    Id = mList.size,
                                    name = it.name,
                                    comment = it.comment,
                                    date = it.date,
                                    isValid = it.isValid,
                                    checks = it.checks
                                )
                            )
                        }
                        addTask.visible(false)
                    }
                }
                JsonHelperInteractor(requireContext()).save(mList)
                adapter.setData(mList)
            }
            else Toast.makeText(requireContext(), "Задача не сохранена", Toast.LENGTH_LONG).show()
        }
    }

    private fun showChooseDialog(item : News, position: Int){
        val options =
            listOf(
                getString(R.string.delete),
                getString(R.string.cancel)
            )
        AlertDialog.Builder(requireContext())
            .also { dialog ->
                val arrayAdapter = ArrayAdapter<String>(requireContext(),
                    R.layout.layout_dialog_list_item
                )
                arrayAdapter.addAll(options)
                dialog.setAdapter(arrayAdapter) { _, which ->
                    when (which) {
                        0 -> showRemoveDialog(item, position)
                        1 -> {}
                    }
                }
            }
            .show()
    }

    private fun showRemoveDialog(item : News, position: Int){
        AlertDialog.Builder(requireContext())
            .setMessage("Удалить задачу \"${item.name}\"?")
            .setPositiveButton("Удалить") {_, _ -> removeItem(position)}
            .setNegativeButton("Отмена") { _, _ -> }
            .show()
    }


    private fun removeItem(position: Int){
        mList.removeAt(position)
        if (mList.isNullOrEmpty()) addTask.visible(true)
        adapter.removeDataWithPosition(position)
        JsonHelperInteractor(requireContext()).save(mList)
    }

    private fun refresh(){
        mSwipeRefreshLayout.setOnRefreshListener {
            mList = JsonHelperInteractor(requireContext()).open()?: mutableListOf()
            adapter.setData(mList)
            mSwipeRefreshLayout.isRefreshing = false
        }
    }

    fun navigateToDetail(item: News){
        (activity as MainActivity)
    }



}