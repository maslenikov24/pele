package com.univer.pele2.activities

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import com.univer.pele2.Check
import com.univer.pele2.News
import com.univer.pele2.R
import com.univer.pele2.fragments.PagerFragment
import kotlinx.android.synthetic.main.activity_content.*
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class ContentActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener{

    private var checks : MutableList<Check>? = mutableListOf()
    private var obj : News? = null
    private var newTask: Boolean = true
    private var date : String? = null

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)


        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentHolder, PagerFragment())
            .addToBackStack(null)
            .commit()
        setSupportActionBar(appBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        appBar.setNavigationOnClickListener {
            onBackPressed()
        }



        appBar.setOnMenuItemClickListener { item ->
            when {
                item.itemId == R.id.home -> {
                    onBackPressed()
                }
                else -> {
                    // do something
                }
            }

            false
        }

        obj = intent.getParcelableExtra<News>("object")

        val name : String
        val comment : String

        if (obj != null){
            newTask = false
            name = obj?.name.toString()
            comment = obj?.comment.toString()
            date = obj?.date
            checks = obj?.checks?.toMutableList()

            label_name.setText(name)
            label_comment.setText(comment)
            label_date.setText(date)
            appBar.title = name

            /*hecks?.forEach {
                addCheckBox(it.name.orEmpty(), it.isChecked)
            }*/


            val answer = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")// HH:mm")
                date?.format(formatter)
            } else {
                val formatter = SimpleDateFormat("MMM dd yyyy")// HH:mma")
                formatter.format(date)
            }

            label_date.setText(answer)
        }
        else appBar.title = "Новая задача"

        label_date.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showDateTimePicker()
            }
        }

        /*fab.setOnClickListener {
            showAlertDialog(it)
        }*/
    }

    private fun newObject() = News(Id = obj?.Id ?: 0,
                name = label_name.text.toString(),
                comment = label_comment.text.toString(),
                date = label_date.text.toString(),
                isValid = obj?.isValid ?: true,
                checks = checks?.let {
                ArrayList(
                    it
                )
            })

    override fun onBackPressed() {
        val intent = Intent (this@ContentActivity, MainActivity::class.java)
        intent
            .putExtra("object", newObject())
        if (newTask) setResult(0, intent)
        else setResult(1, intent)
        super.onBackPressed()
        finish()
    }

    /*private fun addCheckBox(task: String, isChecked : Boolean = false){
        val linearLayout = LinearLayout(this)
        val checkBox = CheckBox(this)
        val checkboxText = EditText(this)
        checkboxText.background = Color.parseColor("#00000000").toDrawable()
        checkboxText.setText(task)
        checkBox.isChecked = isChecked
        checkBox.id
        linearLayout.addView(checkBox)
        linearLayout.addView(checkboxText)
        linear.addView(linearLayout)
        //addTask.visibility = View.GONE
    }

    private fun showAlertDialog(view: View){
        val inflater = LayoutInflater.from(this)
        val dialogLayout = inflater.inflate(R.layout.edittext, null)
        //EditTextCursor(dialogLayout.findViewById<EditText>(R.id.editTextKey))

        val dialog = AlertDialog.Builder(this)
            .setTitle("Новая задача")
            .setView(dialogLayout)
            .setNeutralButton("Отмена") { _, _ -> }
            .setPositiveButton("Ок") { _, _ ->
                val task = dialogLayout.editTextKey.text.toString()
                if (task == "") {
                    val snackbar = Snackbar.make(view, "Нельзя добавть пустую задачу", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                    snackbar.view.setBackgroundColor(Color.parseColor("#ff0000"))
                    snackbar.show()
                }
                else {
                    if (checks.isNullOrEmpty()) checks = mutableListOf(Check(task, isChecked = false))
                    else checks?.add(Check(task, false))
                    addCheckBox(task)
                }
            }.create()

        *//*dialog.setOnShowListener {
            dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(AppThemeAccentColor())
            dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(AppThemeAccentColor())
            EditTextCursor(dialogLayout.editTextKey)

            dialogLayout.editTextKey.background.setColorFilter(AppThemeAccentColor(), PorterDuff.Mode.SRC_IN)
        }*//*

        dialog.show()
    }*/

    private fun showDateTimePicker(){
        val year = date?.split(".")?.get(2)?.toInt()
        val month = date?.split(".")?.get(1)?.toInt()
        val day = date?.split(".")?.get(0)?.toInt()
        DatePickerDialog(
            this,
            this,
            year ?: Calendar.getInstance().get(Calendar.YEAR),
            month ?: Calendar.getInstance().get(Calendar.MONTH),
            day ?: Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        label_date.setText("$dayOfMonth.$month.$year")
    }
}