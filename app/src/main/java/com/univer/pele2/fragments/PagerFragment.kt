package com.univer.pele2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.univer.pele2.JsonHelperInteractor
import com.univer.pele2.News
import com.univer.pele2.R
import com.univer.pele2.TasksStartParams
import com.univer.pele2.activities.MainActivity
import com.univer.pele2.interfaces.TitleNameListener
import kotlinx.android.synthetic.main.fragment_view_pager.*

class PagerFragment : BaseFragment(), TitleNameListener {
    override fun setAppBarTitle(title: String) {
        appBar.title = title
    }

    override val layoutRes = R.layout.fragment_view_pager

    companion object{

        private const val OBJECT = "object"

        fun newInstance(startParams: TasksStartParams) : PagerFragment =
            PagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(OBJECT, startParams.news)
                }
            }
    }

    private val news get() = arguments?.getParcelable<News>(OBJECT)

    private var mList = mutableListOf<News>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //tabs.setupWithViewPager(viewpager)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setSupportActionBar(appBar)

        val pagerAdapter = ScreenSlidePagerAdapter(childFragmentManager)
        viewPager.adapter = pagerAdapter
        viewPager.currentItem = mList.indexOf(news)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getCount(): Int = mList.filter { it.isValid }.count()

        override fun getItem(position: Int): Fragment {
            val validList = mList.filter { it.isValid }
            appBar.title = validList[position].name
            return DetailFragment.newInstance(TasksStartParams(news = validList[position], isLandscape = true))
        }

        //TODO open the desired task

        init {
            mList = JsonHelperInteractor(requireContext()).open()?: mutableListOf()
        }
    }
}