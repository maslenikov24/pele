package com.univer.pele2

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_feed.view.*

interface FeedDelegate{
    fun openDetail(news: News)
    fun isRemoveTask(item: News, position: Int)
    fun completeChange(news: News, isChecked :Boolean, position: Int)
}

class FeedAdapter: RecyclerView.Adapter<FeedAdapter.ViewHolder>() {

    private val mItems = mutableListOf<News>()
    private var delegate: FeedDelegate?= null

    fun attachDelegate(delegate: FeedDelegate){
        this.delegate = delegate
    }

    fun setData(data: MutableList<News>){
        mItems.clear()
        mItems.addAll(data)
        try {
            notifyDataSetChanged()
        } catch (e: Exception) {
            Log.d("Exception", e.toString())
        }

    }

    fun setDataWithPosition(data: News, position: Int){
        mItems.add(position, data)
        try {
            notifyItemInserted(position)
            notifyItemRangeChanged(position, mItems.size)
        } catch (e: Exception) {
            Log.d("Exception", e.toString())
        }

    }

    fun removeDataWithPosition(position: Int){
        mItems.removeAt(position)
        try {
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, mItems.size)
        } catch (e: Exception) {
            Log.d("Exception", e.toString())
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mItems[position], position)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_feed, viewGroup, false), delegate)
    }

    override fun getItemCount(): Int {
        return mItems.count()
    }

    inner class ViewHolder(view: View, private val delegate: FeedDelegate?) : RecyclerView.ViewHolder(view){

        private val name = view.label_name
        private val comment = view.label_comment
        private val date = view.label_date
        private val layout = view.main_Layout
        private val switch = view.mSwitch

        fun bind(item: News, position: Int){
            layout.setOnClickListener {
                if (item.isValid) delegate?.openDetail(item)
            }
            layout.setOnLongClickListener{
                delegate?.isRemoveTask(item, position)
                true
            }
            switch.setOnCheckedChangeListener { buttonView, isChecked ->
                if (buttonView.isPressed) delegate?.completeChange(item, isChecked, position)
            }
            switch.isChecked = !item.isValid
            name.text = item.name
            comment.text = item.comment
            val createDate = "Создано: ${item.date}"
            date.text = createDate

        }
    }
}