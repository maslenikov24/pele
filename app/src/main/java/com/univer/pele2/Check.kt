package com.univer.pele2

import android.os.Parcel
import android.os.Parcelable

data class Check(
    val name : String?,
    val isChecked : Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeByte(if (isChecked) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Check> {
        override fun createFromParcel(parcel: Parcel): Check {
            return Check(parcel)
        }

        override fun newArray(size: Int): Array<Check?> {
            return arrayOfNulls(size)
        }
    }
}